from django.db import models
import requests
from django.conf import settings
from datetime import datetime


DADATA_API_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party'


BUSINESS_ACTIVITY_TYPE_CHOICES = [
    ('individual', 'Физ. Лицо'),
    ('entity', 'Юр. Лицо')
]


class Customer(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя', blank=True)
    inn = models.CharField(max_length=100, blank=True, verbose_name='ИНН')
    ogrn = models.CharField(max_length=100, blank=True, verbose_name='ОГРН')
    ogrn_date = models.DateTimeField(null=True, blank=True, verbose_name='Дата ОГРН')
    business_activity_type = models.CharField(max_length=100, blank=True, verbose_name='Тип деятельности')

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.inn:
                resp = requests.post(DADATA_API_URL, headers={'Content-Type': 'application/json',
                                                              'Accept': 'application/json',
                                                              'Authorization': f'Token {settings.DADATA_KEY}'},
                                     json={'query': self.inn})
                if resp.status_code == 200:
                    if len(resp.json()['suggestions']) != 0:
                        data = resp.json()['suggestions'][0]
                        if 'value' in data:
                            self.name = data['value']
                        if 'ogrn' in data['data']:
                            self.ogrn = data['data']['ogrn']
                        if 'ogrn_date' in data['data']:
                            self.ogrn_date = datetime.fromtimestamp(data['data']['ogrn_date']/1000)
                        if 'type' in data['data']:
                            t = data['data']['type']
                            self.business_activity_type = 'Юридическое лицо' if t == 'LEGAL' else 'Индивидуальный предприниматель'
        super(Customer, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'


class Contractor(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя', blank=True)
    inn = models.CharField(max_length=100, blank=True, verbose_name='ИНН')
    ogrn = models.CharField(max_length=100, blank=True, verbose_name='ОГРН')
    ogrn_date = models.DateTimeField(null=True, blank=True, verbose_name='Дата ОГРН')
    business_activity_type = models.CharField(max_length=100, blank=True, verbose_name='Тип деятельности')

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.inn:
                resp = requests.post(DADATA_API_URL, headers={'Content-Type': 'application/json',
                                                              'Accept': 'application/json',
                                                              'Authorization': f'Token {settings.DADATA_KEY}'},
                                     json={'query': self.inn})
                if resp.status_code == 200:
                    if len(resp.json()['suggestions']) != 0:
                        data = resp.json()['suggestions'][0]
                        if 'value' in data:
                            self.name = data['value']
                        if 'ogrn' in data['data']:
                            self.ogrn = data['data']['ogrn']
                        if 'ogrn_date' in data['data']:
                            self.ogrn_date = datetime.fromtimestamp(data['data']['ogrn_date']/1000)
                        if 'type' in data['data']:
                            t = data['data']['type']
                            self.business_activity_type = 'Юридическое лицо' if t == 'LEGAL' else 'Индивидуальный предприниматель'
        super(Contractor, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Подрядчик'
        verbose_name_plural = 'Подрядчики'
