from rest_framework import generics
from .serializers import *
from rest_framework.permissions import IsAdminUser


class CreateCustomer(generics.CreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [IsAdminUser]


class GetCustomer(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [IsAdminUser]


class DeleteCustomer(generics.DestroyAPIView):
    queryset = Customer.objects.filter()
    serializer_class = CustomerSerializer
    permission_classes = [IsAdminUser]


class UpdateCustomer(generics.UpdateAPIView):
    queryset = Customer.objects.filter()
    serializer_class = CustomerSerializer
    permission_classes = [IsAdminUser]


class CreateContractor(generics.CreateAPIView):
    queryset = Contractor.objects.all()
    serializer_class = ContractorSerializer
    permission_classes = [IsAdminUser]


class GetContractor(generics.ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = ContractorSerializer
    permission_classes = [IsAdminUser]


class DeleteContractor(generics.DestroyAPIView):
    queryset = Contractor.objects.filter()
    serializer_class = ContractorSerializer
    permission_classes = [IsAdminUser]


class UpdateContractor(generics.UpdateAPIView):
    queryset = Contractor.objects.filter()
    serializer_class = ContractorSerializer
    permission_classes = [IsAdminUser]
