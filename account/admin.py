from django.contrib import admin
from .models import *


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Customer._meta.fields]


@admin.register(Contractor)
class ContractorAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Contractor._meta.fields]
