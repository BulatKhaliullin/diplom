from django.urls import path
from .views import *

urlpatterns = [
    path('create_customer/', CreateCustomer.as_view(), name='create_customer'),
    path('get_customer/', GetCustomer.as_view(), name='get_customer'),
    path('delete_customer/<int:pk>/', DeleteCustomer.as_view(), name='delete_customer'),
    path('update_customer/<int:pk>/', UpdateCustomer.as_view(), name='update_customer'),
    path('create_contractor/', CreateContractor.as_view(), name='create_contractor'),
    path('get_contractor/', GetContractor.as_view(), name='get_contractor'),
    path('delete_contractor/<int:pk>/', DeleteContractor.as_view(), name='delete_contractor'),
    path('update_contractor/<int:pk>/', UpdateContractor.as_view(), name='update_contractor'),
]