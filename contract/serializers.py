from rest_framework import serializers
from .models import *


class ContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = '__all__'


class ContractObjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractObject
        fields = '__all__'


class ContractObjectCharSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractObjectChar
        fields = '__all__'


class WorkStageSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkStage
        fields = '__all__'


class MoneyMovementSerializer(serializers.ModelSerializer):
    class Meta:
        model = MoneyMovement
        fields = '__all__'


class ContractGraphicSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractGraphic
        fields = '__all__'


class ContractIncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractIncome
        fields = '__all__'


class ContractExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractExpense
        fields = '__all__'


class PaymentCalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentCalendar
        fields = '__all__'
