from django.contrib import admin
from .models import *


@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Contract._meta.fields]


@admin.register(ContractObject)
class ContractObjectAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ContractObject._meta.fields]


@admin.register(ContractObjectChar)
class ContractObjectCharAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ContractObjectChar._meta.fields]


@admin.register(WorkStage)
class WorkStageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in WorkStage._meta.fields]


@admin.register(MoneyMovement)
class MoneyMovementAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MoneyMovement._meta.fields]


@admin.register(ContractGraphic)
class ContractGraphicAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ContractGraphic._meta.fields]


@admin.register(ContractIncome)
class ContractIncomeAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ContractIncome._meta.fields]


@admin.register(ContractExpense)
class ContractExpenseAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ContractExpense._meta.fields]


@admin.register(PaymentCalendar)
class PaymentCalendarAdmin(admin.ModelAdmin):
    list_display = [field.name for field in PaymentCalendar._meta.fields]
