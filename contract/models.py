from django.db import models
from account.models import *


CONTRACT_TYPE_CHOICES = [
    ('i', 'Исходящий'),
    ('v', 'Входящий')
]


CONTRACT_STATUS_CHOICES = [
    ('in_process', 'В процессе'),
    ('created', 'Создан'),
    ('finished', 'Закончен')
]


class Contract(models.Model):
    name = models.CharField(max_length=100, blank=True, verbose_name='Название')
    customer = models.ForeignKey(Customer, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='contract')
    contractor = models.ForeignKey(Contractor, on_delete=models.DO_NOTHING, blank=True, null=True, related_name='contract')
    contract_type = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField(auto_now=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    description = models.TextField(blank=True)
    price = models.FloatField(default=0)
    status = models.CharField(max_length=100, choices=CONTRACT_STATUS_CHOICES, default='created')
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)


class ContractObject(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    contract = models.OneToOneField(Contract, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='object')


class ContractObjectChar(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)
    contract_object = models.ForeignKey(ContractObject, on_delete=models.CASCADE, blank=True, null=True,
                                        related_name='characteristics')


class WorkStage(models.Model):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=100, blank=True, null=True, choices=CONTRACT_STATUS_CHOICES, default='created')
    price = models.FloatField(default=0)
    payed = models.BooleanField(default=False)


class MoneyMovement(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    contract = models.OneToOneField(Contract, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='money_movement')
    for_what = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField()


GRAPHIC_REPEATING_CHOICES = [
    ('daily', 'Ежедневно'),
    ('weekly', 'Еженедельно'),
    ('monthly', 'Ежемесячно'),
    ('yearly', 'Ежегодно')
]


class ContractGraphic(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    contract = models.OneToOneField(Contract, on_delete=models.CASCADE, blank=True, null=True, related_name='graphic')
    repeat = models.CharField(max_length=100, choices=GRAPHIC_REPEATING_CHOICES)


class ContractIncome(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, blank=True, null=True, related_name='incomes')
    price = models.FloatField(default=0)
    for_what = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now=True, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    money_movement = models.ForeignKey(MoneyMovement, on_delete=models.DO_NOTHING, blank=True, null=True,
                                       related_name='incomes')
    graphic = models.ForeignKey(ContractGraphic, on_delete=models.DO_NOTHING, blank=True, null=True,
                                related_name='incomes')


class ContractExpense(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, blank=True, null=True, related_name='expenses')
    price = models.FloatField(default=0)
    for_what = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now=True, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    money_movement = models.ForeignKey(MoneyMovement, on_delete=models.DO_NOTHING, blank=True, null=True,
                                       related_name='expenses')
    graphic = models.ForeignKey(ContractGraphic, on_delete=models.DO_NOTHING, blank=True, null=True,
                                related_name='expenses')


class PaymentCalendar(models.Model):
    name = models.CharField(max_length=100, blank=True)
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, blank=True, null=True,
                                 related_name='payment_calendar')
    price = models.FloatField(default=0)
    payment_date = models.DateTimeField(blank=True, null=True)
    for_what = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True)
