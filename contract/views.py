from rest_framework import generics
from .serializers import *
from rest_framework.permissions import IsAdminUser
from django.shortcuts import render


''' CONTRACT '''


class CreateContract(generics.CreateAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    permission_classes = [IsAdminUser]


class GetContract(generics.ListAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    permission_classes = [IsAdminUser]


class DeleteContract(generics.DestroyAPIView):
    queryset = Contract.objects.filter()
    serializer_class = ContractSerializer
    permission_classes = [IsAdminUser]


class UpdateContract(generics.UpdateAPIView):
    queryset = Contract.objects.filter()
    serializer_class = ContractSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT OBJECT '''


class CreateContractObject(generics.CreateAPIView):
    queryset = ContractObject.objects.all()
    serializer_class = ContractObjectSerializer
    permission_classes = [IsAdminUser]


class GetContractObject(generics.ListAPIView):
    queryset = ContractObject.objects.all()
    serializer_class = ContractObjectSerializer
    permission_classes = [IsAdminUser]


class DeleteContractObject(generics.DestroyAPIView):
    queryset = ContractObject.objects.filter()
    serializer_class = ContractObjectSerializer
    permission_classes = [IsAdminUser]


class UpdateContractObject(generics.UpdateAPIView):
    queryset = ContractObject.objects.filter()
    serializer_class = ContractObjectSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT OBJECT CHAR '''


class CreateContractObjectChar(generics.CreateAPIView):
    queryset = ContractObjectChar.objects.all()
    serializer_class = ContractObjectCharSerializer
    permission_classes = [IsAdminUser]


class GetContractObjectChar(generics.ListAPIView):
    queryset = ContractObjectChar.objects.all()
    serializer_class = ContractObjectCharSerializer
    permission_classes = [IsAdminUser]


class DeleteContractObjectChar(generics.DestroyAPIView):
    queryset = ContractObjectChar.objects.filter()
    serializer_class = ContractObjectCharSerializer
    permission_classes = [IsAdminUser]


class UpdateContractObjectChar(generics.UpdateAPIView):
    queryset = ContractObjectChar.objects.filter()
    serializer_class = ContractObjectCharSerializer
    permission_classes = [IsAdminUser]


''' WORK STAGE '''


class CreateWorkStage(generics.CreateAPIView):
    queryset = WorkStage.objects.all()
    serializer_class = WorkStageSerializer
    permission_classes = [IsAdminUser]


class GetWorkStage(generics.ListAPIView):
    queryset = WorkStage.objects.all()
    serializer_class = WorkStageSerializer
    permission_classes = [IsAdminUser]


class DeleteWorkStage(generics.DestroyAPIView):
    queryset = WorkStage.objects.filter()
    serializer_class = WorkStageSerializer
    permission_classes = [IsAdminUser]


class UpdateWorkStage(generics.UpdateAPIView):
    queryset = WorkStage.objects.filter()
    serializer_class = WorkStageSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT '''


class CreateMoneyMovement(generics.CreateAPIView):
    queryset = MoneyMovement.objects.all()
    serializer_class = MoneyMovementSerializer
    permission_classes = [IsAdminUser]


class GetMoneyMovement(generics.ListAPIView):
    queryset = MoneyMovement.objects.all()
    serializer_class = MoneyMovementSerializer
    permission_classes = [IsAdminUser]


class DeleteMoneyMovement(generics.DestroyAPIView):
    queryset = MoneyMovement.objects.filter()
    serializer_class = MoneyMovementSerializer
    permission_classes = [IsAdminUser]


class UpdateMoneyMovement(generics.UpdateAPIView):
    queryset = MoneyMovement.objects.filter()
    serializer_class = MoneyMovementSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT GRAPHIC '''


class CreateContractGraphic(generics.CreateAPIView):
    queryset = ContractGraphic.objects.all()
    serializer_class = ContractGraphicSerializer
    permission_classes = [IsAdminUser]


class GetContractGraphic(generics.ListAPIView):
    queryset = ContractGraphic.objects.all()
    serializer_class = ContractGraphicSerializer
    permission_classes = [IsAdminUser]


class DeleteContractGraphic(generics.DestroyAPIView):
    queryset = ContractGraphic.objects.filter()
    serializer_class = ContractGraphicSerializer
    permission_classes = [IsAdminUser]


class UpdateContractGraphic(generics.UpdateAPIView):
    queryset = ContractGraphic.objects.filter()
    serializer_class = ContractGraphicSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT INCOME '''


class CreateContractIncome(generics.CreateAPIView):
    queryset = ContractIncome.objects.all()
    serializer_class = ContractIncomeSerializer
    permission_classes = [IsAdminUser]


class GetContractIncome(generics.ListAPIView):
    queryset = ContractIncome.objects.all()
    serializer_class = ContractIncomeSerializer
    permission_classes = [IsAdminUser]


class DeleteContractIncome(generics.DestroyAPIView):
    queryset = ContractIncome.objects.filter()
    serializer_class = ContractIncomeSerializer
    permission_classes = [IsAdminUser]


class UpdateContractIncome(generics.UpdateAPIView):
    queryset = ContractIncome.objects.filter()
    serializer_class = ContractIncomeSerializer
    permission_classes = [IsAdminUser]


''' CONTRACT EXPENSE '''


class CreateContractExpense(generics.CreateAPIView):
    queryset = ContractExpense.objects.all()
    serializer_class = ContractExpenseSerializer
    permission_classes = [IsAdminUser]


class GetContractExpense(generics.ListAPIView):
    queryset = ContractExpense.objects.all()
    serializer_class = ContractExpenseSerializer
    permission_classes = [IsAdminUser]


class DeleteContractExpense(generics.DestroyAPIView):
    queryset = ContractExpense.objects.filter()
    serializer_class = ContractExpenseSerializer
    permission_classes = [IsAdminUser]


class UpdateContractExpense(generics.UpdateAPIView):
    queryset = ContractExpense.objects.filter()
    serializer_class = ContractExpenseSerializer
    permission_classes = [IsAdminUser]


''' PAYMENT CALENDAR '''


class CreatePaymentCalendar(generics.CreateAPIView):
    queryset = PaymentCalendar.objects.all()
    serializer_class = PaymentCalendarSerializer
    permission_classes = [IsAdminUser]


class GetPaymentCalendar(generics.ListAPIView):
    queryset = PaymentCalendar.objects.all()
    serializer_class = PaymentCalendarSerializer
    permission_classes = [IsAdminUser]


class DeletePaymentCalendar(generics.DestroyAPIView):
    queryset = PaymentCalendar.objects.filter()
    serializer_class = PaymentCalendarSerializer
    permission_classes = [IsAdminUser]


class UpdatePaymentCalendar(generics.UpdateAPIView):
    queryset = PaymentCalendar.objects.filter()
    serializer_class = PaymentCalendarSerializer
    permission_classes = [IsAdminUser]
