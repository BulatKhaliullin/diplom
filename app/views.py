from django.shortcuts import render
import jwt
import time
from django.conf import settings


def main(request):
    payload = {
        "resource": {"question": 1},
        "params": {},
        "exp": round(time.time()) + (60 * 10)}
    token = jwt.encode(payload, settings.METABASE_SECRET_KEY, algorithm="HS256")
    iframe_url = settings.METABASE_SITE_URL + "/embed/question/" + token + "#bordered=true&titled=true"
    return render(request, 'main.html', locals())
